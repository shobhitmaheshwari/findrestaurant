package com.example.shobhit.findrestaurant;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.shobhit.findrestaurant.response.QueryResponse;
import com.example.shobhit.findrestaurant.response.Venue;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class SearchActivity extends AppCompatActivity {

	private LocationData locationData = LocationData.getLocationData();

	//These sre required for working with Foursquare API in unauthenticated mode
	private String CLIENT_ID = "4ZF044PH2XMWG00QSIZQL5PJTEZVGIAYJMY2W2H5Y5BTFO2B";
	private String CLIENT_SECRET = "5V1JYN5BPDVJAI0LYRYKDZMBL54A3GDFGIQHU3CNC3404X5Z";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
	}

	private MyAdapter aa;

	private ArrayList<ListElement> aList;

	@Override
	protected void onResume(){
		Intent intent = getIntent();
		String cuisine = intent.getStringExtra(MainActivity.Cuisine);//get search word

		aList = new ArrayList<ListElement>();
		aa = new MyAdapter(this, R.layout.list_element, aList);
		ListView myListView = (ListView) findViewById(R.id.listView);
		myListView.setAdapter(aa);
		aa.notifyDataSetChanged();

		searchRestaurants(cuisine);//search restaurants serving the cuisine

		super.onResume();
	}

	/**
	 * search restaurants serving the cuisine
	 * @param cuisine
	 */
	private void searchRestaurants(String cuisine){

		HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
		// set your desired log level
		logging.setLevel(HttpLoggingInterceptor.Level.BODY);
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addInterceptor(logging)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl("https://api.foursquare.com/v2/")	//We are using Foursquare API to get data
				.addConverterFactory(GsonConverterFactory.create())	//parse Gson string
				.client(httpClient)	//add logging
				.build();

        FourSquareService service = retrofit.create(FourSquareService.class);

		//convert latitude and longitude into acceptable format
        String ll = Double.toString(locationData.getLocation().getLatitude()) + ","
                + Double.toString(locationData.getLocation().getLongitude());

        Call<QueryResponse> queryResponseCall =
				service.listRestaurants(CLIENT_ID, CLIENT_SECRET, "4d4b7105d754a06374d81259", 20130815, ll, cuisine, 2000);

		//Call retrofit asynchronously
		queryResponseCall.enqueue(new Callback<QueryResponse>() {
			@Override
			public void onResponse(Response<QueryResponse> response) {
				List<Venue> venues = getVenues(response.body());
				aList.clear();
				for(int i = 0; i < venues.size(); i++){
					aList.add(new ListElement(venues.get(i).getName(), "Details"));
				}

				if(venues.size() == 0)
					((TextView) findViewById(R.id.noResult)).setVisibility(View.VISIBLE);//show not found

				// We notify the ArrayList adapter that the underlying list has changed,
				// triggering a re-rendering of the list.
				aa.notifyDataSetChanged();
			}

			@Override
			public void onFailure(Throwable t) {
				// Log error here since request failed
			}

			//parse the response for list of restaurants
			private List<Venue> getVenues(QueryResponse response){
				List<Venue> venues;
				if(response.getMeta().getCode() == 200)
				{
					venues = response.getResponse().getVenues();
				}
				else
					venues = new ArrayList<Venue>();
				return venues;
			}
		});
	}

	/**
	 * Foursquare api https://developer.foursquare.com/docs/venues/search
	 */
	public interface FourSquareService {
		@GET("venues/search")
		Call<QueryResponse> listRestaurants(@Query("client_id") String clientId,
											@Query("client_secret") String client_secret,
											@Query("categoryId") String categoryId,	//catgory id dor Foof
											@Query("v") Integer v,	//version number of Foursquare API
											@Query("ll") String ll,	//latitude and longitude
											@Query("query") String query,	//search term
											@Query("radius") Integer radius);
	}
}