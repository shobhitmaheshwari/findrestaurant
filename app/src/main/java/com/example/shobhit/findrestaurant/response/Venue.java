
package com.example.shobhit.findrestaurant.response;

import java.util.ArrayList;
import java.util.List;
//import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Venue {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contact")
    @Expose
    private Contact contact;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = new ArrayList<Category>();
    @SerializedName("verified")
    @Expose
    private Boolean verified;
    @SerializedName("stats")
    @Expose
    private Stats stats;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("hasMenu")
    @Expose
    private Boolean hasMenu;
    @SerializedName("menu")
    @Expose
    private Menu menu;
    @SerializedName("allowMenuUrlEdit")
    @Expose
    private Boolean allowMenuUrlEdit;
    @SerializedName("specials")
    @Expose
    private Specials specials;
    @SerializedName("hereNow")
    @Expose
    private HereNow hereNow;
    @SerializedName("referralId")
    @Expose
    private String referralId;
    @SerializedName("venueChains")
    @Expose
    private List<VenueChain> venueChains = new ArrayList<VenueChain>();
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("venuePage")
    @Expose
    private VenuePage venuePage;

    /**
     * No args constructor for use in serialization
     *
     */
    public Venue() {
    }

    /**
     *
     * @param venueChains
     * @param location
     * @param stats
     * @param hasMenu
     * @param menu
     * @param allowMenuUrlEdit
     * @param hereNow
     * @param contact
     * @param url
     * @param specials
     * @param referralId
     * @param id
     * @param verified
     * @param venuePage
     * @param name
     * @param categories
     * @param storeId
     */
    public Venue(String id, String name, Contact contact, Location location, List<Category> categories, Boolean verified, Stats stats, String url, Boolean hasMenu, Menu menu, Boolean allowMenuUrlEdit, Specials specials, HereNow hereNow, String referralId, List<VenueChain> venueChains, String storeId, VenuePage venuePage) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.location = location;
        this.categories = categories;
        this.verified = verified;
        this.stats = stats;
        this.url = url;
        this.hasMenu = hasMenu;
        this.menu = menu;
        this.allowMenuUrlEdit = allowMenuUrlEdit;
        this.specials = specials;
        this.hereNow = hereNow;
        this.referralId = referralId;
        this.venueChains = venueChains;
        this.storeId = storeId;
        this.venuePage = venuePage;
    }

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The contact
     */
    public Contact getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     *     The contact
     */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     *
     * @return
     *     The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @param location
     *     The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     *
     * @return
     *     The categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     *
     * @param categories
     *     The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     *
     * @return
     *     The verified
     */
    public Boolean getVerified() {
        return verified;
    }

    /**
     *
     * @param verified
     *     The verified
     */
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    /**
     *
     * @return
     *     The stats
     */
    public Stats getStats() {
        return stats;
    }

    /**
     *
     * @param stats
     *     The stats
     */
    public void setStats(Stats stats) {
        this.stats = stats;
    }

    /**
     *
     * @return
     *     The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     *     The url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     *     The hasMenu
     */
    public Boolean getHasMenu() {
        return hasMenu;
    }

    /**
     *
     * @param hasMenu
     *     The hasMenu
     */
    public void setHasMenu(Boolean hasMenu) {
        this.hasMenu = hasMenu;
    }

    /**
     *
     * @return
     *     The menu
     */
    public Menu getMenu() {
        return menu;
    }

    /**
     *
     * @param menu
     *     The menu
     */
    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    /**
     *
     * @return
     *     The allowMenuUrlEdit
     */
    public Boolean getAllowMenuUrlEdit() {
        return allowMenuUrlEdit;
    }

    /**
     *
     * @param allowMenuUrlEdit
     *     The allowMenuUrlEdit
     */
    public void setAllowMenuUrlEdit(Boolean allowMenuUrlEdit) {
        this.allowMenuUrlEdit = allowMenuUrlEdit;
    }

    /**
     *
     * @return
     *     The specials
     */
    public Specials getSpecials() {
        return specials;
    }

    /**
     *
     * @param specials
     *     The specials
     */
    public void setSpecials(Specials specials) {
        this.specials = specials;
    }

    /**
     *
     * @return
     *     The hereNow
     */
    public HereNow getHereNow() {
        return hereNow;
    }

    /**
     *
     * @param hereNow
     *     The hereNow
     */
    public void setHereNow(HereNow hereNow) {
        this.hereNow = hereNow;
    }

    /**
     *
     * @return
     *     The referralId
     */
    public String getReferralId() {
        return referralId;
    }

    /**
     *
     * @param referralId
     *     The referralId
     */
    public void setReferralId(String referralId) {
        this.referralId = referralId;
    }

    /**
     *
     * @return
     *     The venueChains
     */
    public List<VenueChain> getVenueChains() {
        return venueChains;
    }

    /**
     *
     * @param venueChains
     *     The venueChains
     */
    public void setVenueChains(List<VenueChain> venueChains) {
        this.venueChains = venueChains;
    }

    /**
     *
     * @return
     *     The storeId
     */
    public String getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     *     The storeId
     */
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     *     The venuePage
     */
    public VenuePage getVenuePage() {
        return venuePage;
    }

    /**
     *
     * @param venuePage
     *     The venuePage
     */
    public void setVenuePage(VenuePage venuePage) {
        this.venuePage = venuePage;
    }

}
